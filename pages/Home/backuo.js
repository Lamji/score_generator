import { React, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Paper, Typography, TextField, Button} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Swal from 'sweetalert2'


import AppHelper from '../../app-helper'
import moment from 'moment'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        width: '50%',
        margin: '30px auto',
        [theme.breakpoints.down('md')]: {
            width: '100%',
            padding: '10px'
        },
        '& .MuiOutlinedInput-input': {
            padding: '10px',
            fontSize: '12px'
        },
        '& .MuiInputBase-root': {
            backgroundColor: 'white',
            '& fieldset': {
               
            },
            '&.Mui-focused fieldset': {
                border: '1px solid gray',
            }
        },
        "& input::-webkit-clear-button, & input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button": {
            display: "none",
            margin: 80
        },
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        backgroundColor: theme.palette.text.secondary,
        boxShadow: 'none',
        margin: '10px 0',
        color: 'white'
    },
    Input: {
        marginRight: "5px",
        [theme.breakpoints.down('md')]: {
            margin: '0',
            marginBottom: '7px'
        },
    },
    Result: {
        border: '1px solid #aaaaaa',
        borderRadius: '6px',
        textAlign: 'left',
        margin: '10px 0',
        padding: '10px'
    },
    subTitle: {
        fontSize: '10px',
        margin: '5px 0'
    },
    Table:{
        fontSize: '12px',
        padding: '5px',
        border: '1px solid #aaaaaa',
        height: '30px'
    },
    TableHeaderContainer:{
        padding: '0 25px 0 10px',
        [theme.breakpoints.down('md')]: {
            padding: '0 10px'
        },
    },
    TableHeader:{
        fontSize: '12px',
        padding: '5px 0',
        border: '1px solid #aaaaaa',
        height: '30px',
        textAlign: 'center'
    },
    TableRaw:{
        fontSize: '12px',
        padding: '10px',
        '& .MuiSelect-select':{
            fontSize: '12px'
        }
    },
    ResultHolder:{
        color: theme.palette.text.secondary,
        boxShadow: 'none',
        padding: '5px 0'
    },
    ResultContainer:{
        height: '200px',
        overflowY: 'scroll',
        padding: '0 10px'
    },
    Loading: {
        margin: "100px auto",
        textAlign: 'center'
    }
}));

export default function CenteredGrid() {
    const classes = useStyles();

    const [start, setStart] = useState('')
    const [end, setEnd] = useState('')
    const [result, setResult] = useState(0)
    const [data,setData] = useState([])
    const [status,setStatus] = useState(false)
    const [generated,setGenerated] =useState('')
    const [date,setDate] =useState([])
    const [isExist,setIsExsist] =useState(true)
    const [dateExist,setDateExist] =useState(0)
    const [selectDate,setSelectDate] =useState([])
    const [filterDate,setFilterDate] =useState([])
    const [dateNow,setDateNow] = useState(moment(Date()).format('MMMM DD YYYY'))

    const handleChange = (event) => {
      setDateNow(event.target.value);
    };

    useEffect(() => {
        fetch(`${AppHelper.API_URL}/details`,)
        .then(res => res.json())
        .then(result => {
            setData(result.reverse())
        })
   
        setResult(getRandomInt(parseInt(start),parseInt(end)))
 
        setDate(data.map(res => {
            return moment(res.Date).format('MMMM DD YYYY')
        }))
 
        const filterDate = [...new Set(date)]

        setSelectDate(filterDate)
   
        setFilterDate(data.filter(res => {
            return moment(res.Date).format('MMMM DD YYYY') === dateNow
        }))
        setDateExist(selectDate.filter( res => {return res == moment(Date()).format('MMMM DD YYYY')}))
        console.log(dateExist)
    },[data])
    
    useEffect(() => {
        console.log(isExist)
    },[])

    const Reset = () => {
        setResult(0)
        setStart('')
        setEnd('')
      
    }

    const Generate = (e) => {
        e.preventDefault()
        if(start >= end) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Start number must be smaller than ending number',
              })
        }else{
            setStatus(false)
            setGenerated(result)
            AddScore()
        }
    }

    const AddScore = () =>{
      
        fetch(`${AppHelper.API_URL}/addScore`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                Score: result,
                Count: data.filter(res => {
                    return res.Scores === result
                }).length+1,
                Date: new Date(),
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                setStatus(true)
            }
        })
        //Updating count
        fetch(`${AppHelper.API_URL}/updateCount`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                Score: result,
                Count: data.filter(res => {
                    return res.Scores === result
                }).length+1,
              
            })
        })
        .then(res => res.json())
        .then(data => {})
    }

 

   
    function getRandomInt(min, max) {                                   
        return Math.floor(Math.random() * (max - min + 1) + min);
      }

    return (
        <div className={classes.root}>
            <Grid container>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        <Typography variant="h5">
                            Random Score Generator
                        </Typography>
                        <Typography variant="body1" className={classes.subTitle}>
                            *Start number must be less than ending number
                        </Typography>
                        <form onSubmit={(e) => Generate(e)}>
                            <Grid container
                                direction="row"
                                justify="center"
                                alignItems="center"
                                spacing={1}>
                                    <Grid item xs={8} container spacing={1}>
                                        <Grid item xs={12}>
                                            <TextField required id="From" variant="outlined" placeholder="Input a starting number" type="number" value={start} onChange={e => setStart(e.target.value)} fullWidth />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField required id="To" variant="outlined" placeholder="Input a ending number" type="number" value={end} onChange={e => setEnd(e.target.value)} fullWidth />
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <Button className={classes.Button} variant="contained" color="primary" type="submit" id="submitBtn">
                                            Generate
                                        </Button>
                                    </Grid>
                            </Grid>
                        </form>
                    </Paper>
                    <Grid container direction="row"
                        justify="center"
                        alignItems="center">
                        <Button className={classes.ButtonReset} variant="contained" color="secondary" onClick={e => Reset(e)}>
                            Reset
                        </Button>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid
                    className={classes.Result}
                    container
                    direction="row"
                    justify="center"
                    alignItems="center">
                        {generated === '' ? <Typography variant='body1'>0</Typography> : 
                            <Typography>{status ? generated : "Generating ...."}</Typography>
                        }
                    </Grid>
                    <Grid container
                    direction="row"
                    justify="flex-start"
                    alignItems="center"
                    className={classes.TableRaw}>
                        <Grid item xs={5}><Typography variant="body1">Result History</Typography></Grid>
                        <Grid item xs={7}>
                        <TextField
                        fullWidth
                        id="date"
                        select
                        onChange={handleChange}
                        SelectProps={{
                            native: true,
                          }}
                          helperText="Select a date">
                        {selectDate.map((option) => (
                            <option key={option} value={option} className={classes.option}>
                                {option}
                            </option>
                        ))}
                        </TextField>
                        </Grid>
                    </Grid>
            
                    <Grid container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    className={classes.TableHeaderContainer}>
                        <Grid item xs={2}> <Typography variant="body1" className={classes.TableHeader}>Score</Typography></Grid>
                        <Grid item xs={4}> <Typography variant="body1" className={classes.TableHeader}>Generated per day</Typography></Grid>
                        <Grid item xs={6}> <Typography variant="body1" className={classes.TableHeader}>Date generated</Typography></Grid>
                    </Grid>
                    <Grid item className={classes.ResultContainer}>
                        {isExist ? 
                        <Grid>
                            {filterDate == 0 ? <Typography className={classes.Loading}>Getting data . . </Typography> : null }
                            {filterDate.map(res => {
                                    return (
                                        <Grid container
                                        direction="row"
                                        justify="flex-start"
                                        alignItems="center"
                                        key={res._id} >
                                            <Grid item xs={2}> <Typography variant="body1" className={classes.Table}>{res.Scores}</Typography></Grid>
                                            <Grid item xs={4}> <Typography variant="body1" className={classes.Table}>{res.Count}</Typography></Grid>
                                            <Grid item xs={6}> <Typography variant="body1" className={classes.Table}>{moment(res.Date).format('MMMM DD YYYY HH:mm')}</Typography></Grid>
                                        </Grid>
                                    )
                            })}
                        </Grid>
                        : <Typography variant="body1" className={classes.Loading}>No record history for this day.</Typography> }
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}

